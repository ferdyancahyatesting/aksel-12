const request = require('supertest');
const app = 'https://reqres.in/api';

// API TESTING [PUT]
describe('Update data user', () => {
it('[PUT] Update User', async () => {
    const response = await request(app)
      .put('/users/2')
      .send({ name: 'Ferdyan Kece', job: 'Pendekar' });

    expect(response.status).toEqual(200);
    expect(response.body.name).toEqual('Ferdyan Kece');
    expect(response.body.job).toEqual('Pendekar');
  });
});