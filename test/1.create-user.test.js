const request = require('supertest');
const app = 'https://reqres.in/api';

// API TESTING [POST]
describe('Create a new user', () => {
  test('[POST] Create User', async () => {
    const response = await request(app)
      .post('/users')
      .send({ name: 'Yohanes Ferdyan Cahya Buana', job: 'SQA' });

    expect(response.status).toEqual(201);
    expect(response.body.name).toEqual('Yohanes Ferdyan Cahya Buana');
    expect(response.body.job).toEqual('SQA');
  });
})