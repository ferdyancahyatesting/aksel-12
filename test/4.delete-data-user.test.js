const request = require('supertest');
const app = 'https://reqres.in/api';

// API TESTING [DELETE]
describe('Delete user data', () => {
it('[DELETE] Delete User', async () => {
    const response = await request(app).delete('/users/2');

    expect(response.status).toEqual(204);
  });
});