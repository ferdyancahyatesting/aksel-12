const request = require('supertest');
const app = 'https://reqres.in/api';

// API TESTING [GET]
describe('Search Data User', () => {
it('[GET] Get Users', async () => {
    const response = await request(app).get('/users?page=2');

    expect(response.status).toEqual(200);
    expect(response.body.page).toEqual(2);
    expect(response.body.data.length).toBeGreaterThan(0);
  });
});