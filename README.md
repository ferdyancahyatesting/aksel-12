# privy-jest
- Run All test suite with `jest` or `npm run jest` command

- For run specific test with `jest filename.test.js` or `npm run jest filename.test.js`

- Test report will be saved to test-report.html

- Open test-report.html with your favorite browser
